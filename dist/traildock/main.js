var traildockRedrawInterval = null;
document.addEventListener("DOMContentLoaded", function (e) {
  document.getElementById('sidebar').style.top = '60px';
  document.getElementById('sidebar').style.height = 'calc(100% - 60px)';
  var div = document.createElement('div');
  div.style.position = 'absolute';
  div.style.top = '0';
  div.style.left = '0';
  div.style.height = '60px';
  div.style.width = '33.333333%';
  div.style['max-width'] = '400px';
  div.style['background-color'] = '#fff';
  div.style['z-index'] = '10000';
  div.style['border-bottom'] = '1px solid #ededed';
  div.style['padding'] = '10px';
  div.innerHTML = '<a href="/"><img style="height:30px;width:auto;" src="//cdn.traildock.com/images/traildock-text.png" /></a>';
  document.body.appendChild(div);
});
