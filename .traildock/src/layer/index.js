import _debounce from 'lodash-es/debounce';

import { traildockMarkers } from './markers';

export function traildockLayer(projection, context, dispatch) {
    var enabled  = true;
    var markers  = traildockMarkers(projection, context, dispatch);

    function drawTraildock(selection) {
        if (context.map().zoom() < 10) return;
        selection.selectAll('.layer-traildock')
            .data(['markers'])
            .enter()
            .append('g')
            .attr('class', function(d) { return 'layer-traildock layer-' + d; });

        markers.redraw(selection);
    }

    function showLayer() {
        var layer = context.surface().selectAll('.data-layer-traildock');
        layer.interrupt();

        layer
            .classed('disabled', false)
            .style('opacity', 0)
            .transition()
            .duration(250)
            .style('opacity', 1)
            .on('end interrupt', function () {
                dispatch.call('change');
            });
    }

    function hideLayer() {
        var layer = context.surface().selectAll('.data-layer-traildock');
        layer.interrupt();

        layer
            .transition()
            .duration(250)
            .style('opacity', 0)
            .on('end interrupt', function () {
                layer.classed('disabled', true);
                dispatch.call('change');
            });
    }

    drawTraildock.enabled = function(_) {
        if (!arguments.length) return enabled;
        enabled = _;

        if (enabled) {
            showLayer();
        } else {
            hideLayer();
        }

        dispatch.call('change');
        return this;
    };

    return drawTraildock;
}
