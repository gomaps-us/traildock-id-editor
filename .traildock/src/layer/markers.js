import _debounce from 'lodash-es/debounce';
import { svgPointTransform } from '../../../modules/svg';
import { select as d3_select } from 'd3-selection';

var apibase = 'https://api.traildock.com/',
    collections = ['facility', 'features', 'observations', 'reports', 'signs', 'trailheads'];

export function traildockMarkers (projection, context, dispatch) {

  var _this = this;
  this.requestRunning = false;
  this.markers = [];

  this.redraw = function (selection) {
    _this.getMarkers(function (data) {
      var formatted = {
        facilities: _this.formatData(data[0], 'facilities'),
        features: _this.formatData(data[1], 'features'),
        observations: _this.formatData(data[2], 'observations'),
        reports: _this.formatData(data[3], 'reports'),
        signs: _this.formatData(data[4], 'signs'),
        trailheads: _this.formatData(data[5], 'trailheads')
      }
      _this.render(selection, formatted);
    });
    _this.render(selection);
  };

  this.getBoundingBox = function () {
    var extent = context.extent();
    return {
      swLng: extent[0][0],
      swLat: extent[0][1],
      neLng: extent[1][0],
      neLat: extent[1][1]
    };
  };

  this.formatData = function (data, type) {
    for (var i = 0; i < data.length; i++) {
      data[i].loc = [ data[i].geometry.coordinates[0], data[i].geometry.coordinates[1] ]
      var subtype = _this.getTagValue(data[i].tags ? data[i].tags : [], 'subtype');
      data[i].type = subtype ? subtype : type;
    }
    return data;
  };

  this.getTagValue = function (tags, name) {
    for (var i = 0; i < tags.length; i++) {
      if (tags[i].split(':')[0] == name) {
        return tags[i].split(':')[1];
      }
    }
    return null;
  };

  this.render = function (selection, data) {
    var container = selection.selectAll('.layer-traildock.layer-markers');
        //container.attr('transform', 'translate(-50, -60)');
    if (data) {
      for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        for (var i = 0; i < data[key].length; i++) {
          _this.markers.push(drawMarker(container, data[key][i].id, data[key][i].loc, data[key][i].type, data[key][i]));
        }
      }
    } else {
      for (var i = 0; i < _this.markers.length; i++) {
        drawMarker(container, _this.markers[i].id, _this.markers[i].loc, _this.markers[i].type, _this.markers[i].data);
      }
    }
  };

  this.drawMarker = function (container, id, loc, type, data) {
    var transform = projection(loc);
    var existing = container.select('#traildock-marker-' + id);
    if (existing.size() > 0) {
      existing.attr('transform', 'translate(' + transform[0] +',' + transform[1] + ')');
    } else {
      var g = container.append('g')
                .attr('id', 'traildock-marker-' + id)
                .attr('class', 'traildock-marker')
                .attr('transform', 'translate(' + transform[0] +',' + transform[1] + ')')
                .attr('style', 'cursor:pointer;')
                .on('click', _this.selectMarker);
      switch (type) {
        case 'trailpost': {
          g.append('path')
            .attr('transform', 'translate(-8, -23) scale(0.5)')
            .attr('fill', '#B3B284')
            .attr('d', ('M76.4,47.8v8.8c0,1.2-1,2.2-2.2,2.2H28c-0.9,0-1.7-0.3-2.3-1L20.8,53c-0.4-0.4-0.4-1.1,0-1.6l4.8-4.8c0.6-0.6,1.5-1,2.3-1\
              h17.6V39h8.8v6.6h19.8C75.4,45.6,76.4,46.6,76.4,47.8z M79.2,31l-4.8,4.8c-0.6,0.6-1.5,1-2.3,1H25.8c-1.2,0-2.2-1-2.2-2.2v-8.8\
              c0-1.2,1-2.2,2.2-2.2h19.8v-2.2c0-1.2,1-2.2,2.2-2.2h4.4c1.2,0,2.2,1,2.2,2.2v2.2H72c0.9,0,1.7,0.3,2.3,1l4.8,4.8\
              C79.6,29.9,79.6,30.6,79.2,31z M54.4,61v17.6c0,1.2-1,2.2-2.2,2.2h-4.4c-1.2,0-2.2-1-2.2-2.2V61H54.4z').replace(/\n\s+/g, ' '))
            .attr('stroke', '#4D2C10')
            .attr('stroke-width', '1');
          break;
        }
        case 'signs': {
          g.append('path')
            .attr('transform', 'translate(-80, -100) scale(1.8)')
            .attr('stroke', '#FFFFFF')
            .attr('fill', '#ED0626')
            .attr('stroke-width', '0.5')
            .attr('d', 'M47.1,56.4v-0.7c1.2-0.1,1.3-0.2,1.3-1.7V50c0-1.4-0.1-1.4-1.1-1.6v-0.6c1.2-0.1,2.6-0.4,3.7-0.7v7c0,1.4,0.1,1.5,1.3,1.7v0.7H47.1z');
          g.append('path')
            .attr('transform', 'translate(-80, -100) scale(1.8)')
            .attr('stroke', '#FFFFFF')
            .attr('fill', '#ED0626')
            .attr('stroke-width', '0.5')
            .attr('d', 'M48.1,44.4c0-0.8,0.7-1.4,1.5-1.4c0.8,0,1.5,0.6,1.5,1.4c0,0.8-0.6,1.4-1.5,1.4C48.7,45.8,48.1,45.2,48.1,44.4z');
          break;
        }
        case 'reports': {
          g.append('path')
            .attr('transform', 'translate(-8, -23) scale(1)')
            .attr('stroke', '#2E7828')
            .attr('stroke-width', '1')
            .attr('fill', '#419639')
            .attr('fill-rule', 'nonzero')
            .attr('d', 'M1.33333333,27.6333211 L30.6666667,27.6333211 L16,2.29998779 L1.33333333,27.6333211 Z M17.3333333,23.6333211 L14.6666667,23.6333211 L14.6666667,20.9666545 L17.3333333,20.9666545 L17.3333333,23.6333211 Z M17.3333333,18.2999878 L14.6666667,18.2999878 L14.6666667,11.6333211 L17.3333333,11.6333211 L17.3333333,18.2999878 Z');
          break;
        }
      }

    }
    return {id: id, loc: loc, type: type, data: data};
  };

  this.getMarkers = _debounce(function (callback) {
    var url = apibase + 'aggregate/' + collections.join(',') + '/within';
    var boundingBox = _this.getBoundingBox();
    _this.request(
      url + '?southWestLongitude=' + boundingBox.swLng +
            '&southWestLatitude=' + boundingBox.swLat +
            '&northEastLongitude=' + boundingBox.neLng +
            '&northEastLatitude=' + boundingBox.neLat,
      callback
    );
  }, 1000);

  this.selectMarker = function (event) {
    var id = d3_select(this).attr('id').split('-')[2];
    var data = null;
    for (var i = 0; i < _this.markers.length; i++) {
      if (_this.markers[i].data.id == id) {
        data = _this.markers[i].data;
        break;
      }
    }
    if (data) {
      var html = '';
      if (data.files && data.files.length > 0) {
        for (var ii = 0; ii < data.files.length; ii++) {
          html += '<div><img style="width:100%;" src="' + data.files[ii] + '" /></div>';
        }
        document.getElementById('sidebar').querySelector('.inspector-body').innerHTML = html;
      }
    }
  }

  this.request = function (endpoint, callback) {
    if (_this.requestRunning) return;
    _this.requestRunning = true;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      _this.requestRunning = false;
      if (request.readyState === 4) {
        if (request.status < 400) {
          var data = JSON.parse(request.responseText);
          if (callback) callback(data);
        }
      }
    };
    request.open('GET', endpoint);
    request.send();
  };

  return this;
};
