'use strict';

var fs    = require('fs');
var path  = require('path');

var action = process.argv.length >= 3 ? process.argv[2] : null;

if (!fs.existsSync(path.resolve(__dirname, 'actions', action + '.js'))) {
  console.error('Invalid action: ' + action);
  process.exit(1);
}

require('./actions/' + action)(process.argv);
