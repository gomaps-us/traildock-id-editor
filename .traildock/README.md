# Traildock iD Editor

We've tried to reduce overlap of efforts going on in the upstream project (https://github.com/openstreetmap/iD), so as much as we can possibly isolate and put in this directory for the sake of the functionality specific to Traildock, the better.

## Upstreams

This fork is based on both the [main OSM branch of the iD editor](https://github.com/openstreetmap/iD), but also [Strava's fork](https://github.com/strava/iD) that has implemented some important features, namely [the Slide Tool](https://labs.strava.com/slide/). As a developer on this fork, you should have both OSM's and Strava's repos as upstreams in your local clone:

```
origin	git@bitbucket.org:gomaps-us/traildock-id-editor.git (fetch)
origin	git@bitbucket.org:gomaps-us/traildock-id-editor.git (push)
upstream	git@github.com:openstreetmap/iD.git (fetch)
upstream	git@github.com:openstreetmap/iD.git (push)
upstream-strava	git@github.com:strava/iD.git (fetch)
upstream-strava	git@github.com:strava/iD.git (push)
```

### Traildock-specific npm scripts

Pulling in changes from OSM's most-recent master branch:
```
npm run traildock -- rebase
```
If you need to resolve conflicts after running the above, it'll need to be done manually. We hope we've extended everything here where the case of that happening will be minimal.

Building and releasing the Docker image:
```
npm run traildock -- build docker
npm run traildock -- release
```
