'use strict';

var fs                = require('fs');
var path              = require('path');
var imageryPath       = path.resolve(__dirname, '..', '..', 'data', 'imagery.json');
var imagery           = require(imageryPath);
var traildockImagery  = require(path.resolve(__dirname, '..', 'data', 'imagery.json'));
var cheerio           = require('cheerio');
var beautify          = require('js-beautify').html;
var shell             = require('shelljs');

var updateIndexHtml = function (htmlPath, distPath) {
  shell.exec('git checkout upstream/master ' + htmlPath, {
    cwd: path.resolve(__dirname, '..', '..')
  });
  var index = cheerio.load(fs.readFileSync(htmlPath));
  index('head title').text('Traildock iD');
  index('head title').after('<link rel="icon" type="img/ico" href="//cdn.traildock.com/images/favicon.ico">');
  index('body').append('<script src="' + distPath + 'traildock/main.js"></script>');
  fs.writeFileSync(htmlPath, beautify(index.html(), {
    html: {
      indent_inner_html: true,
      extra_liners: []
    },
    indent_size: 4
  }));
}

function imageryIdExists (id) {
  for (var i = 0; i < imagery.dataImagery.length; i++) {
    if (imagery.dataImagery[i].id == id) return true;
  }
  return false;
}

function getAsset(type, subtype) {
  var asset = fs.readFileSync(path.resolve(__dirname, '..', 'assets', type, (subtype ? subtype : 'default') + '.svg'))
           .toString()
           .replace(/\n|\t/g, ' ');
  while (asset.match(/\s{2}/g)) {
    asset = asset.replace(/\s{2}/g, ' ');
  }
  asset = asset.substring(asset.search(/<g[\s>]/g), asset.lastIndexOf('</g>') + 4).replace(/filter="[^"]+"/g, '').replace(/\s+>/g, '>');
  return asset;
}

module.exports = function (args) {

  // traildock js
  if (!fs.existsSync(path.resolve(__dirname, '..', '..', 'dist', 'traildock'))) {
    fs.mkdirSync(path.resolve(__dirname, '..', '..', 'dist', 'traildock'));
  }
  fs.createReadStream(path.resolve(__dirname, '..', 'src', 'traildock.js')).pipe(
    fs.createWriteStream(path.resolve(__dirname, '..', '..', 'dist', 'traildock', 'main.js'))
  );

  // update index.html files
  updateIndexHtml(path.resolve(__dirname, '..', '..', 'index.html'), 'dist/');
  updateIndexHtml(path.resolve(__dirname, '..', '..', 'dist', 'index.html'), '');

  // write updated imagery file
  shell.exec('git checkout upstream/master data/imagery.json', {
    cwd: path.resolve(__dirname, '..', '..')
  });
  for (var i = 0; i < traildockImagery.dataImagery.length; i++) {
    if (!imageryIdExists(traildockImagery.dataImagery[i].id)) {
      imagery.dataImagery.push(traildockImagery.dataImagery[i]);
    }
  }
  fs.writeFileSync(imageryPath, JSON.stringify(imagery, null, 4));

  // roll up assets
  var assets = {
    'facilities': getAsset('facilities'),
    'facilities_campground': getAsset('facilities', 'campground'),
    'facilities_shelter': getAsset('facilities', 'shelter'),
    'facilities_toilet': getAsset('facilities', 'toilet'),
    'features': getAsset('features'),
    'features_water': getAsset('features', 'water'),
    'observations': getAsset('observations'),
    'reports': getAsset('reports'),
    'signs': getAsset('signs'),
    'signs_trailpost': getAsset('signs', 'trailpost'),
    'trailheads': getAsset('trailheads'),
  };
  fs.writeFileSync(path.resolve(__dirname, '..', 'assets', 'assets.json'), JSON.stringify(assets, null, 2));

  if (args.indexOf('docker') >= 0) {
    // rebuild docker image
    setTimeout(function () {
      var dockerFile = fs.readFileSync(path.resolve(__dirname, '..', 'Dockerfile'));
      fs.writeFileSync(path.resolve(__dirname, '..', '..', 'Dockerfile'), dockerFile);
      shell.exec('docker build -t traildock/id-editor:stable .', {
        cwd: path.resolve(__dirname, '..', '..')
      });
      fs.unlinkSync(path.resolve(__dirname, '..', '..', 'Dockerfile'));
    }, 1000);
  }

};
