'use strict';

var path  = require('path');
var shell = require('shelljs');

var resetFiles = [
  path.resolve(__dirname, '..', '..', 'index.html'),
  path.resolve(__dirname, '..', '..', 'dist', 'index.html'),
  path.resolve(__dirname, '..', '..', 'data', 'imagery.json'),
];

module.exports = function(args) {

  var result = shell.exec('git diff-index HEAD', {
    silent: true,
    cwd: path.resolve(__dirname, '..', '..')
  });
  if (result.stdout.toString() != '') {
    console.error("There are unstaged/uncommitted changes, you need to commit those before doing a rebase\n");
    process.exit(1);
  }
  result = shell.exec('git remote -v', {
    silent: true,
    cwd: path.resolve(__dirname, '..', '..')
  });
  if (!result.stdout.toString().match(/upstream\s+/)) {
    console.error("There is no upstream remote in your repo, you need to add it like 'git add remote upstream git@github.com:openstreetmap/iD.git'\n");
    process.exit(1);
  }

  // fetch OSM master state
  result = shell.exec('git fetch upstream');
  if (result.code != 0) {
    console.error(result.stderr);
    process.exit(result.code);
  }

  // reset to OSM master state
  resetFiles.forEach(function (file) {
    shell.exec('git checkout upstream/master ' + file);
    shell.exec('git add ' + file);
  });
  result = shell.exec('git commit -m "traildock rebase: reset certain files to OSM state prior to rebase"');

  // rebase on OSM master
  result = shell.exec('git rebase upstream/master');
  if (result.code != 0) {
    console.error(result.stderr);
    process.exit(result.code);
  }

  // now for getting back up-to-speed with traildock needed changes
  result = shell.exec('npm run traildock -- build');
  if (result.code != 0) {
    console.error(result.stderr);
    process.exit(result.code);
  }
  result = shell.exec('git add .');
  if (result.code != 0) {
    console.error(result.stderr);
    process.exit(result.code);
  }
  result = shell.exec('git commit -m "traildock rebase: re-applied traildock changes to certain files"');
  if (result.code != 0) {
    console.error(result.stderr);
    process.exit(result.code);
  }

};
