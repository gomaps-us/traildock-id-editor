'use strict';

var fs      = require('fs');
var path    = require('path');
var shell   = require('shelljs');

module.exports = function (args) {

  shell.exec('docker push traildock/id-editor', {
    cwd: path.resolve(__dirname, '..', '..')
  });

};
